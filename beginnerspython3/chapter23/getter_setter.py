class Person:
    def __init__(self, name):
        self._name = name
        self._age = None

    def get_age(self):
        return self._age

    def set_age(self, new_age):
        if isinstance(new_age, int) & (new_age > 0) & (new_age < 120):
            self._age = new_age


person = Person('John')
person.set_age(54)
print(person.get_age())
person.set_age(64)
print(person.get_age())
