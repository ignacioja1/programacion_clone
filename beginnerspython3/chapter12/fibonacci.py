# Function for nth fibonacci number

# Using a recursive function
import numpy as np


def fib_recursive(n: int) -> int:
    """
    Recursive function to calculate the n-th fibonacci number
    :param n:   position in the sequence
    :return:    Fn
    """
    if n == 0:
        # By definition
        return 0
    elif n == 1:
        # By definition
        return 1
    else:
        return fib_recursive(n - 1) + fib_recursive(n - 2)


# Time Complexity: T(n) = T(n-1) + T(n-2) which is exponential.

# fibonacci Series using Dynamic Programming

def fib_dynamic_array(n: int) -> int:
    """
    fibonacci Series using Dynamic Programming, using array
    :param n:   position in the sequence
    :return:    Fn
    """
    if n < 2:
        return n
    table = np.zeros(shape=(n + 1,), dtype=int)
    table[1] = 1
    for i in range(2, n + 1):
        table[i] = table[i - 1] + table[i - 2]
    return table[n]


def fib_dynamic_list(n):
    """
    fibonacci Series using Dynamic Programming, using list
    :param n:   position in the sequence
    :return:    Fn
    """
    if n < 2:
        return n
    results = [0, 1]
    for i in range(2, n + 1):
        results.append(results[i - 1] + results[i - 2])
    return results[-1]


# Function for nth fibonacci number - Space Optimisataion, Iterative
# Taking 1st two fibonacci numbers as 0 and 1

def fib_dynamic_iter(n):
    """
    fibonacci Series using Dynamic Programming, iterative approach
    :param n:   position in the sequence
    :return:    Fn
    """
    previous, current = (0, 1)
    for i in range(2, n + 1):
        previous, current = (current, previous + current)
    return current


# Time
# Complexity: O(n)
# Extra
# Space: O(1)

# https://en.wikipedia.org/wiki/Fibonacci_number#Binet's_formula

def fib_formula_2(n: int) -> int:
    """
    fibonacci Series using Binets Formula
    :param n:   position in the sequence
    :return:    Fn
    """
    phi = (1 + 5 ** 0.5) / 2
    return round(phi ** n / 5 ** 0.5)


if __name__ == '__main__':
    N = 17
    REPEATS = 10000
    import timeit

    print(f"fib_recursive({N})")
    print(timeit.timeit(f"fib_recursive({N})",
                        setup="from __main__ import fib_recursive",
                        number=REPEATS))

    print(f"fib_dynamic_array({N})")

    setup = """from __main__ import fib_dynamic_array;import numpy as np"""

    print(timeit.timeit(f"fib_dynamic_array({N})",
                        setup=setup,
                        number=REPEATS))

    print(f"fib_dynamic_list({N})")
    print(timeit.timeit(f"fib_dynamic_list({N})",
                        setup="from __main__ import fib_dynamic_list",
                        number=REPEATS))

    print(f"fib_dynamic_iter({N})")
    print(timeit.timeit(f"fib_dynamic_iter({N})",
                        setup="from __main__ import fib_dynamic_iter",
                        number=REPEATS))

    print(f"fib_formula_2({N})")
    print(timeit.timeit(f"fib_formula_2({N})",
                        setup="from __main__ import fib_formula_2",
                        number=REPEATS))
