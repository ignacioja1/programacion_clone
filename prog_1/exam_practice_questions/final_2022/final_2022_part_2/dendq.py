from dataclasses import dataclass
from collections import deque


@dataclass
class Student:
    name: str
    grades: list[float]


@dataclass
class StudentQueue(deque):
    pass

    def add_student(self, name: str, grades: list[float]):
        self.append(Student(name, grades))

    def pop_student(self):
        s: Student = self.popleft()
        print(f'Student {s.name} with average grade {sum(s.grades)/len(s.grades)}')


# FIFO
q = StudentQueue()
q.add_student('bob', [8., 9.])
q.add_student('mary', [6., 10.])
q.pop_student()
