class Dog:
    def __init__(self,
                 name: str,
                 color: str,
                 tricks: set = None):
        """
        Create a Dog object
        :param name: name of the dog
        """
        self.name = name
        self.color = color
        if tricks is None:
            self.tricks = set()
        else:
            self.tricks = tricks

    def __str__(self):
        return f'A dog named {self.name}'

    def add_tricks(self, trick: str) -> None:
        """
        Adds a trick to a Dog
        :param trick:   a trick to add
        """
        self.tricks.add(trick)


d = Dog('Firulai', 'brown')
t = Dog('Teo', 'black')
t.add_tricks('shakes_hand')
t.add_tricks('barks')

print('barks' in t.tricks)
