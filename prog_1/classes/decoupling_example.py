import datetime as dt

class WayPoint:
    def __init__(self, lat: float, lon: float, time: dt.datetime):
        self.lat = lat
        self.lon = lon
        self.time = time

class Delivery:
    def __init__(self, driver: str, wp: WayPoint):
        self.driver = driver
        self.wp = wp

    @classmethod
    def from_values(cls, driver: str, lat: float, lon: float,
                    year: int, month: int, day: int):
        time = dt.datetime(year, month, day)
        return cls(driver, WayPoint(lat=lat,
                                    lon=lon,
                                    time=time))

class Report:
    def __init__(self, report: str, level: int, wp: WayPoint):

        self.report = report
        self.level = level
        self.wp = wp


d = Delivery("Pepe", WayPoint(40.3, 2.71, dt.datetime(2021, 1, 1)))
d.wp.time.day


e = Delivery.from_values("Pepe", 40.3, 2.71, 2021, 1, 1)