class User:
    user_list = []

    def __init__(self, user_id: str, name: str):
        self.user_id = user_id
        self.name = name
        self.printed = None

    def print(self):
        self.printed = True
        print(self.name)

    @classmethod
    def new_user(cls, user_id: str, name: str) -> 'User':
        cls.user_list.append(user_id)
        return cls(user_id, name)


u1 = User.new_user('123', 'bob')
u2 = User.new_user('125', 'mary')
print(User.user_list)


