# Adapted from https://medium.com/better-programming/advanced-python-9-best-practices-to-apply-when-you-define-classes-871a27af658b

class Student:
    def __init__(self,
                 first_name: str,
                 last_name: str,
                 grades: dict[str, float]):

        self.first_name = first_name
        self.last_name = last_name
        self.grades = grades

    def mean(self):
        return sum(self.grades.values())/len(self.grades)

    def __repr__(self):
        return f"Student({self.first_name!r}, {self.last_name!r})"

    def __str__(self):
        return f"Student: {self.first_name} {self.last_name}"


student1 = Student("David", "Peterson", {'prog1': 4.0, 'prog2': 7.0})
student2 = Student("John", "Doe", {'prog1': 8.0, 'prog2': 7.5, 'algo': 9.0})

d = {
    student1.last_name: student1,
    student2.last_name: student2
}

print(d["Peterson"].grades["prog1"])

print(d["Peterson"].mean())
