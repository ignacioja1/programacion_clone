from dataclasses import dataclass
from math import log2


@dataclass
class DomainError(Exception):
    message: str


def protected_log2(x: float) -> float:
        if x > 0:
            return log2(x)
        else:
            raise DomainError(str(x))


while True:
    try:
        x_f = float(input('Input a number: '))
        print(protected_log2(x_f))
    except ValueError:
        print('Invalid input, cannot convert to float')
    except DomainError as de:
        print(f'Invalid input {de.message}, domain error')
    else:
        break

