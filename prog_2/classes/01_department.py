class Department:
    def __init__(self, name: str, department_code: str):
        self.name = name
        self.department_code = department_code

        self.courses = {}

    def add_course(self,
                   course_description: str,
                   course_code: str,
                   n_credits: int):
        course = Course(course_description, course_code, n_credits, self.department_code)
        self.courses[course.course_code] = course
        return self.courses[course.course_code]


class Course:
    def __init__(self,
                 course_description: str,
                 course_code: str,
                 n_credits: int,
                 department_code: str):
        self.course_description = course_description
        self.course_code = course_code
        self.credits = n_credits
        self.department_code = department_code

        self.classes = set([])


maths_dept = Department("Mathematics and Applied Mathematics", "MAM")
mam1000w = maths_dept.add_course("Mathematics 1000", "MAM1000W", 1)

print('end')
