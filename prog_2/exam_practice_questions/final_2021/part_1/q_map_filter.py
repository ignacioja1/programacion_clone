cities = {'Wales':  ('Cardiff', 0.335),
          'England': ('London', 8.982),
          'Scotland': ('Edinburgh', 0.482),
          'Northern Ireland': ('Belfast', 0.280),
          'Ireland': ('Dublin', 0.544)}

# The filter
list(filter(lambda x: x[1] > 0.4, cities.values()))

# The flter and the map
list(map(lambda x: x[0], filter(lambda x: x[1] > 0.4, cities.values())))

# List comprenehsion
l = [city for city, pop in cities.values() if pop > 0.4]

# A bit more complicated (was not part of the exam)
list(map(lambda x: x[0], filter(lambda kv: kv[1][1] > 0.4, cities.items())))
l2 = [region for region, city in cities.items() if city[1] > 0.4]