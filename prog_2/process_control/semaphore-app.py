from threading import Thread, Lock, currentThread
from time import sleep


def worker(lck):
    with lck:
        print(currentThread().getName() + " - entered")
        sleep(0.5)
        print(currentThread().getName() + " - exiting")



print('MainThread - Starting')

lock = Lock()
for i in range(0, 5):
    thread = Thread(name='T' + str(i), target=worker, args=[lock])
    thread.start()

print('MainThread - Done')

