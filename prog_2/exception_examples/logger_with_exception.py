import math
import logging
# Empty basic config turns off default console handler
logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
# create file handler which logs to the specified file
file_handler = logging.FileHandler('calc_log.log')
logger.addHandler(file_handler)
# Create formatter for the file_handler
formatter = logging.Formatter('%(asctime)s.%(msecs)03d [%(levelname)s] %(module)s: %(funcName)s: %(message)s')
file_handler.setFormatter(formatter)


def calc_log(x):
    logger.debug('calling calc_log')
    return math.log(x)


try:
    calc_log(-2)
    logger.info('Done')
except ValueError as e:
    logger.error(e)

