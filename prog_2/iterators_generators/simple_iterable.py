shopping_list = ['ham', 'spam', 'jam']
it = iter(shopping_list)
it.__next__()  # same as next(it)
next(it)
next(it)
next(it)

# Simple iterations with iterables

for element in [1, 2, 3]:
    print(element)
for element in (1, 2, 3):
    print(element)
for key in {'one':1, 'two':2}:
    print(key)
for char in "123":
    print(char)
for line in open("myfile.txt"):
    print(line, end='')

